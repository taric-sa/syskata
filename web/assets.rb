require 'sprockets'

module Syskata
  class Web
    configure do
      set :assets, Sprockets::Environment.new { |env|
        env.append_path('./web/assets')
      }
    end

    get "/assets/*" do |logical_path|
      asset = settings.assets.find_asset(logical_path, {})

      if asset
        if env["HTTP_IF_NONE_MATCH"] == %("#{asset.digest}")
          304
        else
          content_type(asset.content_type)
          headers 'Etag' => %("#{asset.digest}")
          asset
        end
      end
    end
  end
end
