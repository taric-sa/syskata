# Syskata

## Requisitos

* JRuby
* bundler `gem install bundler`

### Empaquetado

* git
* bower `npm install -g bower` (ver sección entorno si se prefiere usar con Docker)
* Maven

### Servicios

* postgresql

## Entorno

Para actualizar todas las dependencias:

    $ bundle install
    $ bower install
    $ mvn clean process-sources # Instalar dependencias Java

Alternativamente se puede usar un Bower de Docker:

    $ docker run --rm -ti -v `pwd`:/srv marmelab/bower bash -c "bower --allow-root --config.interactive=false install"

### rbenv

El repositorio está preparado para trabajar con [rbenv](https://github.com/sstephenson/rbenv).

Se aconseja el uso del mismo con el plugin [ruby-build](https://github.com/sstephenson/ruby-build).

En ese caso una vez clonado actualizar la versión de ruby y asegurarse que se tiene bundler:

    $ rbenv install
    $ gem install bundler
    $ rbenv rehash

Puede ser necesario hacer otro rehash tras el `bundle install` si se han añadido dependencias con
binarios que se desen usar (por ej. para la documentación)

### Reportes

La aplicación incluye los fuentes de algunos reportes que se desean incluir. Se tienen que compilar con rake:

    $ rake reports:compile

## Base de datos

    $ sudo -u postgres psql
    postgres# CREATE USER syskata WITH PASSWORD 'syskata';
    postgres# CREATE DATABASE syskata OWNER syskata;

### Inicializar la base de datos

    $ rake db:migrate

## Arrancar la aplicación

    $ bundle exec rackup
