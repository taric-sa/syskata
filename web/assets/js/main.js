var syskata = angular.module('Syskata', []);
syskata.controller('Main', ['$scope', '$http', function ($scope, $http) {
  var refresh = function(){
    $http.get('/api/users').then(function(resp){$scope.users = resp.data});
  };

  refresh();
  $scope.user = {};

  $scope.delete_user = function(id, index) {
    $http.delete('/api/users/'+id).then(function(resp){$scope.users.splice(index, 1)})
  };

  $scope.add_user = function() {
    $http.post('/api/users', angular.toJson($scope.user)).then(function(resp){ refresh(); $scope.user = {}; });
  };
}]);
