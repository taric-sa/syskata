Sequel.migration do
  up do
    self[:users].multi_insert([
      {name: 'Ada Lovelace', email: 'ada@example.com'},
      {name: 'Marie Curie', email: 'marie@example.com'},
      {name: 'Inge Lehmann', email: 'inge@example.com'}
    ])
  end
end
