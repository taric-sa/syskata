require 'syskata/config'
require 'sequel'

# Load plugins/extensions for every model
Sequel::Model.plugin :json_serializer
Sequel.extension :migration

module Syskata
  module DatabaseService
    CONFIG = Syskata.config.get_settings('database.yml')
    ENCODING = 'UTF-8'

    class << self
      def startup
        @db ||= Sequel.connect(
          adapter: 'jdbc',
          database: CONFIG['database'],
          user: CONFIG['user'],
          password: CONFIG['password'],
          encoding: ENCODING
        )
        @db.test_connection
        self
      end

      def updated?
        migrator = Sequel::IntegerMigrator.new(db, migrations_dir)
        unless migrator.is_current?
          raise "Database is not updated"
        end
      end

      def migrate
        migrator = Sequel::IntegerMigrator.new(db, migrations_dir)
        migrator.run unless migrator.is_current?
        self
      end

      def migrations_dir
        File.join(Syskata.config.root, "db")
      end

      def disconnect
        @db.disconnect
      end

      def db
        @db
      end
    end
  end
end
