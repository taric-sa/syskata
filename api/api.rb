require 'sinatra'
require 'json'

module Syskata
  class Api < Sinatra::Base
    configure do
      set :show_exceptions, false
    end

    before do
      content_type :json
    end

    get '/about/?' do
      {
        name: PRODUCT_NAME,
        version: VERSION,
        environment: Syskata.config.env
      }.to_json
    end

    get '/users/?' do
      DatabaseService.db[:users].all.to_json
    end

    post '/users/?' do
      DatabaseService.db[:users].insert(JSON.parse(request.body.read))
      201
    end

    delete '/users/:id/?' do
      DatabaseService.db[:users].where(id: params[:id]).delete
      200
    end
  end
end
