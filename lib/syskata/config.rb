require 'yaml'

module Syskata
  def self.config
    root = ::File.expand_path('../..', File.dirname(__FILE__))
    @config ||= Config.new(root, ENV['RACK_ENV'])
  end

  class Config
    # @param root [String] Application root path
    # @param environment [String] Running environment (should be RACK_ENV variable)
    def initialize(root, environment = nil)
      @env = ENV['RACK_ENV'] = environment || 'development'
      @root = root
    end

    # @return [String] configuration directory path
    def config_dir
      @config_dir ||= get_config_dir
    end

    # @return [String] application data directory path
    def app_data_dir
      @app_data_dir ||= get_app_data_dir
    end

    # Load settings from a file
    # @param filename [String] settings file name (settings.yml by default)
    # @return settings, a processed yaml file
    def get_settings(filename = 'settings.yml')
      ::YAML.load_file(find_yaml(filename))[env]
    end

    # Execute a block on specified environment
    # @param *args [Array<Symbol>] Target environments
    # @example Execute on test and development environments
    #  Abstra.config.on :test, :development do
    #    #some code
    #  end
    def on(*args)
      if args.include? env.to_sym
        yield
      end
    end

    attr_reader :env, :root

    protected

    # Returns name from @app_name class instance variable
    def app_name
      'syskata'
    end

    def get_config_dir
      if ('production' == env)
        "/etc/#{app_name}"
      else
        File.join(@root, 'config')
      end
    end

    def get_app_data_dir
      if ('production' == env)
        "/var/opt/#{app_name}"
      else
        File.join(@root, 'data')
      end
    end

    # Search a yaml in config directory. It tries to find first a customized file (.yml.custom extension)
    # @param filename [String] config file name
    # @return [String] full file path
    def find_yaml(filename)
      config_file = File.join(config_dir, filename)
      custom_config_file = config_file + '.custom'

      # Use custom file if exist, else find regular file
      file = (File.exists?(custom_config_file) && custom_config_file) ||
        (File.exists?(config_file) && config_file)
      raise "ERROR: Missing config file: #{config_file}" unless file

      file
    end
  end
end
