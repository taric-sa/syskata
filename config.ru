require 'bundler/setup'

$: << File.join(__dir__,'.')
$: << File.join(__dir__,'lib')

ENV['RACK_ENV'] ||= 'development'

require 'syskata_app'

run Syskata::Web
map('/api'){ run Syskata::Api }
