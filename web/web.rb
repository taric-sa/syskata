require 'sinatra'
require 'json'

module Syskata
  class Web < Sinatra::Base
    set :show_exceptions, :after_handler

    # Main route
    get '/?' do
      erb(:main, layout: :'layouts/main')
    end

    get '/users.pdf/?' do
      content_type 'application/pdf'
      String.from_java_bytes(DatabaseService.db.synchronize do |jdbc_connection|
        jasper_print = Java::NetSfJasperreportsEngine::JasperFillManager.fill_report(File.join(Syskata.config.app_data_dir,'syskata.jasper'), {}, jdbc_connection)
        Java::NetSfJasperreportsEngine::JasperExportManager.export_report_to_pdf(jasper_print)
      end)
    end

    error 500 do |ex|
      [500, erb(:error, locals: {code: 500, msg: ex.message}, layout: :'layouts/main')]
    end

    error Sinatra::NotFound do
      [404, erb(:error, locals: {code: 404, msg: 'El recurso no se ha encontrado'}, layout: :'layouts/main')]
    end
  end
end

require_relative 'assets'
