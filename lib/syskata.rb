jar_dir = File.join(__dir__, '/syskata/java/*.jar')
Dir[jar_dir].each do |jar|
  require jar
end

require 'syskata/database'
Syskata::DatabaseService.startup
Syskata::DatabaseService.updated?

require 'syskata/version'
require 'syskata/config'
